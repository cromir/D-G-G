# Player Unknowns Battlegrounds

[Home](../README.md) > [Player Unknowns Battlegrounds](README.md) > Hardcore Rules

![Image](http://cdn.akamai.steamstatic.com/steam/apps/578080/header.jpg?t=1516879634)

## Hardcore Rules

* You drink 2 every time you go down
* If you die first you drink 3
* If all squad member go down before the first one dies everybody drinks 3
* If the zone kills you, you drink 3
* If you make a kill the rest of the squad drink one
