# Player Unknowns Battlegrounds

[Home](../README.md) > [Player Unknowns Battlegrounds](README.md) > Advanced Rules

![Image](http://cdn.akamai.steamstatic.com/steam/apps/578080/header.jpg?t=1516879634)

## Advanced Rules

* You drink every time you go down
* If you die first you drink 2 
* If all squad member go down before the first one dies everybody drinks 2
* If the zone kills you, you drink two
