# Player Unknowns Battlegrounds

[Home](../README.md) > Player Unknowns Battlegrounds

![Image](http://cdn.akamai.steamstatic.com/steam/apps/578080/header.jpg?t=1516879634)

* [Easy Rules](Easy.md)
* [Advanced Rules](Advanced.md)
* [Hardcore Rules](Hardcore.md)
