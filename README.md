# Drunken Games

These are a few drinking games you can play with friends (if you have friends (actually you don't need friends, but that would be sad...)).

## Games
* [Counter Strike Global Offensive](CSGO/README.md)
* [Deep Rock Galactic](Deep%20Rock%20Galactic/README.md)
* [Garry's mod](Gmod/README.md)
* [Player Unknowns Battlegrounds](PUBG/README.md)
* [Planetary Annihilation](Planetary%20Annihilation/README.md)
* [Rainbow Six Siege](RSS/README.md)
* [War Thunder](War%20Thunder/README.md)
