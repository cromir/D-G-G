# Garry's Mod

[Home](../README.md) > [Garry's Mod](../README.md) > **Trouble in Terrorist Town** | Advanced Rules

![Image](http://cdn.edgecast.steamstatic.com/steam/apps/4000/header.jpg?t=1497714104)

## **Trouble in Terrorist Town** | Advanced Rules

- You drink every time you die
- You drink if you make a kill
- You drink twice if you get knifed
- You drink twice if you are traitor and get killed
