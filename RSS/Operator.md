# Rainbow Six Siege

[Home](../README.md) > [Rainbow Six Siege](README.md) > Operator Specific Rules

![Image](http://cdn.akamai.steamstatic.com/steam/apps/359550/header.jpg?t=1516728408)

## Operator Specific Rules
Here are Rules for each Operator

### Vigil
* You drink 2 if a drone spots you

### Dokkaebi
* You drink 2 if you activate your gadget and nobody dies

### Zofia
* You drink one for each gadget you use

### Ela
* You drink 1 for each trap, that isn't triggered

### Ying
* You drink 2 for each friend you flash

### Lesion
* You drink 1 for each Lesion that is triggered

### Mira
* You drink 2 for each mirror who gets destroied

### Jackal
* You drink 1 if you make an assist kil by spotting

### Hibana
* You drink 1 for each X-Ray Load you blow up

### Echo
* You drink 3 if your drone gets destroied

### Caveira
* You drink 1 for each interrogation

### Capitão
* You drink 1 for each kill you make with your gadget

### Blackbeard
* You drink 2 for each shield who gets destroied

### Valkyrie
* You drink 1 for each camera that gets destroied

### Buck
* You drink 2 for each shotgun kill you make

### Frost
* You drink 1 for each trap who gets activted

### Mute
* You drink 1 for each drone you block

### Sledge
* You drink 1 each time you use your hammer

### Smoke
* You drink 2 for each kill you make with your gadget

### Thatcher
* You drink 1 for each device you kill with your EMP-Grenades

### Ash
* You drink one for each explosion that is caused by you

### Castle
* You drink 1 for each extended barricade who gets destried

### Pulse
* You drink 1 for each spotting assistant kill

### Thermite
* You drink 1 for each thermal charge you use

### Montagne
* You drink 1 for each freind that dies

### Twitch
* You drink 2 for each drone you loose

### Doc
* You drink 1 for each healing you use on yourself

### Rook
* You drink 1 for each armor that is not picked up

### Jäger
* You drink 1 for each blocked grenade

### Bandit
* You drink 1 for each electricial device which gets destroied

### Blitz
* You drink 1 for each time you blitz someone

### IQ
* You drink 1 for each spotted device

### Fuze
* You drink 2 for each brech charge which doesnt kill at least one enemy

### Glaz
* You drink for each scope kill you make

### Tachanka
* You drink 2 if you dont make at least one kill with your LMG

### Kapkan
* You drink for each trap which is activated
