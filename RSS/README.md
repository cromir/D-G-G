# Rainbow Six Siege

[Home](../README.md) > Rainbow Six Siege

![Image](http://cdn.akamai.steamstatic.com/steam/apps/359550/header.jpg?t=1516728408)

* [Easy Rules](Easy.md)
* [Advanced Rules](Advanced.md)
* [Hardcore Rules](Hardcore.md)
* [Operator Rules](Operator.md)
