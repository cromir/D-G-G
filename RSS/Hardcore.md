# Rainbow Six Siege

[Home](../README.md) > [Rainbow Six Siege](README.md) > Hardcore Rules

![Image](http://cdn.akamai.steamstatic.com/steam/apps/359550/header.jpg?t=1516728408)

## Hardcore Rules

* You drink 2 if you die
* You drink 2 if you make a kill
* You drink 3 if you get knifed
* You drink 4 if Caveira interrogates you
