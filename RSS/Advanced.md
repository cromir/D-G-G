# Rainbow Six Siege

[Home](../README.md) > [Rainbow Six Siege](README.md) > Advanced Rules

![Image](http://cdn.akamai.steamstatic.com/steam/apps/359550/header.jpg?t=1516728408)

## Advanced Rules

- You drink every time you die
- You drink if you make a kill
- You drink twice if you get knifed
